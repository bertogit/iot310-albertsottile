This was an expirement with Tensorflow and Machine Learning.  I used the model included in the Tensorflow tutorial for Image Recognition.  It was very extensive and was actually very good at indentifying objects.  The original version of "Classify_image.py" allowed for image names to be sent as arguments.  The python program would match the images based on the model and identified them.  I followed a separate tutorial to modify the "Classify_Image.py" program to enable the picamera to take pictures of objects and a voice software to say what the object was.  I included some pictures and videos showing the the program's effectiveness at identifying objects.

I had hoped to have more time to try to incorporate this into an IoT edge server like we had done with the “MeNotMe” project, and use the excellent model already trained by the Tensorflow group. Also, I had hoped to possibly train my own models for things using the Tensorflow software. 

Overall, I found this final project to be a good culmination of what we learned toward the end of this class. Before this class, I had no concept of Tensorflow, Nueral Networks or Machine Learning. Now I am amazed by the possibilities and plan to continue working on new identification projects like “Eyeglasses or not”, “Animal Identification" or "Text Readers". I would also like to research some other uses for Tensorflow pertaining to speech recognition, song sampling, etc.


The Process:
install tensorflow models for python 2.7
https://github.com/samjabrahams/tensorflow-on-raspberry-pi

followed image recognition tutorial 
https://www.tensorflow.org/tutorials/image_recognition

test image recognition
python classify_image.py --image_file "cat.jpg"

modified classified_image.py based on this tutorial to add voice and picamera
https://planb.nicecupoftea.org/2016/11/11/a-speaking-camera-using-pi3-and-tensorflow/

