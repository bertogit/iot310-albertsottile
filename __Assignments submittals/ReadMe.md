# Summary of IOT310

Week 01:
*	Run Electron locally; MQT; RPI sensehat

Week 02:
*	Azure Portal CLI
*	Deploy virtual Linux machine; run nginx on it; destroy
*	Clone Azure sample “Hello World” app
*	Deploy Web App; edit hello world; git push to resource; destroy
*	To destroy, delete the resource group; this will delete everything in that group.

Week 03:
*	Using Azure web interface instead of CLI
*	“+ Create A Resource”:  search for a resource to add; “Blades” are the windows that popup as you click on things
*	Note: Creating Resources:  may need to clear browser cache if get Rain Cloud 
*	Create an IoT Hub; RPI sensehat sends data; device to cloud
1.	Create a resource group
2.	Create an IoT Hub; “F1 - Pricing Tier”; 
3.	Settings > Shared Access Policy > iothubowner > get access keys > connection string
4.	Settings > IoT Devices > Add Device > Get Primary Access Key
- Authentication Type = Symmetric Key, Autogenerate Keys
5.	Run python client app on RPI; using access key for IoT Device
-	python app.py '<IoT Device access key>'
6.	View Azure portal metrics
7.	Listen to messages using iothub-explorer; uses connection string for iothubowner; (device-to-cloud)
8.	Send info to RPI from cloud; (cloud-to-device)
9.	Use VSCode azure toolkit to listen from within VSCode

Week 04:
*	Use Docker as a container for different VMs; run locally; provides a prompt in the VM container
*	Then run Docker in Azure resource
1.	Create a resource group
2.	Create a linux app service plan
3.	Create a web app
4.	Push Docker container

Week 05 / 06:
*	Create Azure Container instance and observer its properties
1.	Create a resource group
2.	Create a container
3.	Run command to see the FQDN
4.	Pull logs
5.	Attach to output streams; attaches to local console output
6.	Destroy
*	Processing data from RPI with IoT Hub and Azure
1.	Create a resource group
2.	Create an IoT Hub (see instructions above in week 03)
3.	Create a Function App
-	OS = Windows, hostage Plan = Consumption Plan, Storage = Create New (give name), Application Insights = Off
4.	Create new function from template
-	Select newly created function app > Functions > ‘+ New Function’ (top of page)
-	select IoT Hub (Event Hub)
-	Language = Javascript
-	New Connection > IoT Hub; select the iothub created above; Endpoint = Events 
5.	Create a new Cosmos DB database (takes a while to create)
-	API = SQL, no geo-redundancy
6.	Bind DB to Function
-	Integrate tab > Output > Azure Cosmos DB
-	Account connection ! > new > select DB just created (takes a while)
-	Checkbox “if true creates the Azure Cosmos DB and collection”
7.	Copy javascript example for reading RPI data (see lab instructions); copy code into Function > Save ; Test javscript by clicking Run
8.	Start python client from week 03 
-	(https://github.com/richardjortega/rpi-client-sensehat)
-	python app.py '<IoT Device access key>'
9.	View Collection in Cosmos DB > Data Explorer > MyCollection > Documents 
10.	Blob Storage - Creating a storage account
-	Note: A storage account should have been automatically created during function app creation.  If it was, then skip this section; if not create a new storage account using settings below.
-	Deployment model = Resource Manager, Account kind = storage V2, replication = locally redundant, Performance = standard, Access tier = Hot, Source transfer = disabled, virtual networks = disabled
-	Use resource group that contains the iothub
11.	Create custom endpoint
-	Iothub > Endpoints > Add
-	Endpoint type = Azure storage container
-	select blob storage created earlier > add a container (if not already created)
-	select container for the storage (ie. “evthub”)
12.	Create routes for data storage
-	Messaging > Routes > Add
-	Data Source = Device Messages, Endpoint = (endpoint just created), Enable Rule = On, query string = “true” (no quotes)
13.	Open software, Microsoft Azure Storage Explorer to view the DB
-	Access Azure account
-	Storage accounts > storage account just created > Blob containers > events
-	Takes some time; default 100 seconds to write to blob (setup during endpoint config)

Week 07:
*	Mobile Applications sing Ionic Framework; creates a web app that shows mqtt subscription packets; can also integrate with xcode to create an iphone app; need mac virtual machine to run xcode.
*	Install ionic on laptop from bash CLI (see lab instructions)
*	Create “hello world”; bash CLI; run app
*	Create “Tabs” template project; bash CLI; run app
-	Do not install “free Ionic Pro SDK”
-	Edit TypeScript file in “CitizenApp”; paste javascript excerpt from week01/lab02; correct errors as explained in lab instructions
-	Install MQTT
-	edit citizenapp/…home.html and replace div <ion-content padding> with excerpt in lab instructions
-	run app again; also run sensehat app on RPI (centralEdgeV1-ClientPublisher.py)
-	see MQTT in ionic app
*	use xcode to create and app version; (skipped this because don’t have mac machine for xcode)
