# Summary of Week 05 and 06 

Week 05 / 06:
*	Create Azure Container instance and observer its properties
1.	Create a resource group
2.	Create a container
3.	Run command to see the FQDN
4.	Pull logs
5.	Attach to output streams; attaches to local console output
6.	Destroy
*	Processing data from RPI with IoT Hub and Azure
1.	Create a resource group
2.	Create an IoT Hub (see instructions above in week 03)
3.	Create a Function App
-	OS = Windows, hostage Plan = Consumption Plan, Storage = Create New (give name), Application Insights = Off
4.	Create new function from template
-	Select newly created function app > Functions > ‘+ New Function’ (top of page)
-	select IoT Hub (Event Hub)
-	Language = Javascript
-	New Connection > IoT Hub; select the iothub created above; Endpoint = Events 
5.	Create a new Cosmos DB database (takes a while to create)
-	API = SQL, no geo-redundancy
6.	Bind DB to Function
-	Integrate tab > Output > Azure Cosmos DB
-	Account connection ! > new > select DB just created (takes a while)
-	Checkbox “if true creates the Azure Cosmos DB and collection”
7.	Copy javascript example for reading RPI data (see lab instructions); copy code into Function > Save ; Test javscript by clicking Run
8.	Start python client from week 03 
-	(https://github.com/richardjortega/rpi-client-sensehat)
-	python app.py '<IoT Device access key>'
9.	View Collection in Cosmos DB > Data Explorer > MyCollection > Documents 
10.	Blob Storage - Creating a storage account
-	Note: A storage account should have been automatically created during function app creation.  If it was, then skip this section; if not create a new storage account using settings below.
-	Deployment model = Resource Manager, Account kind = storage V2, replication = locally redundant, Performance = standard, Access tier = Hot, Source transfer = disabled, virtual networks = disabled
-	Use resource group that contains the iothub
11.	Create custom endpoint
-	Iothub > Endpoints > Add
-	Endpoint type = Azure storage container
-	select blob storage created earlier > add a container (if not already created)
-	select container for the storage (ie. “evthub”)
12.	Create routes for data storage
-	Messaging > Routes > Add
-	Data Source = Device Messages, Endpoint = (endpoint just created), Enable Rule = On, query string = “true” (no quotes)
13.	Open software, Microsoft Azure Storage Explorer to view the DB
-	Access Azure account
-	Storage accounts > storage account just created > Blob containers > events
-	Takes some time; default 100 seconds to write to blob (setup during endpoint config)

