
** "albertornot"- Redoing menotme from week 8 **

"Create an IoT Hub" ___________________
- create resource group in South Central US to match location of customvision machine learning location
- add iothub to it; select Free Tier (F1)
- add iotedge device to it; (iot edge preview; not iot device)

"Start IoT Edge runtime" ___________________
- FIRST update setup tools
sudo pip install --upgrade setuptools pip
- then install iotedge runtime
sudo pip install -U azure-iot-edge-runtime-ctl
- setup iotedge runtime
sudo iotedgectl setup --connection-string "HostName=albertornot-iothub.azure-devices.net;DeviceId=albertornot-iotedge;SharedAccessKey=<#################>" --nopass
- start iotedge runtime
sudo iotedgectl start
- check that docker is running
sudo docker ps

"How to build a classifier with Custom Vision"  ___________________

- sign in to customvisionai.com
- create a new project
- select "limited trial" (Free); doesnt necessarily have to be associated with existing azure account
- upload images
- Train it
- Performance tab > Export > Tensorflow
- get downloaded zip (4418dad313274f018e476e57a250c338)
- extract and change names; Rename labels.txt to model.pbtxt
- move the files model.pb and model.pbtxt to path below on RPI replacing existing
~/week8/CustomVisionContainer/models_classify/

"Section 4: Wiring it up!" __________________
- *(only if necessary) Stop all docker containers and remove all docker containers
sudo docker stop $(sudo docker ps -aq)
sudo docker rm $(sudo docker ps -aq)

- Build the CustomVisionContainer; had to run docker build as sudo
- use my Docker Hub username
cd CustomVisionContainer
sudo docker build --file Dockerfile.arm32v7 -t bertodock/customvisionmodule:0.0.13-arm32v7 .
sudo docker push bertodock/customvisionmodule:0.0.13-arm32v7
- Build the StartupContainer; had to run docker build as sudo
cd StartupContainer
sudo docker build --file Dockerfile.arm32v7 -t bertodock/startupmodule:0.0.7-arm32v7 .
sudo docker push bertodock/startupmodule:0.0.7-arm32v7

- Edit "deployment.json"
- replace "richardjortega" with "bertodock"
- save and load to clouddrive in azure portal
- Apply the configuration to your IoT Edge device
az iot hub apply-configuration --device-id "albertornot-iotedge" --hub-name "albertornot-iothub" --content deployment.json

- view the logs
sudo docker logs -f edgeAgent

________________________________________

Troubelshooting... 
- had to remove nginx becuase it was preventing edgehub on port 8883
sudo apt-get purge nginx nginx-common
sudo apt-get autoremove

Ultimately need to have 4 docker containers running (see screenshot)
- StartupModule, CustomVisionModule, azureiotedge-hub, azureiotedge-agent

Better learning on the model would make it identify better
need more pictures of "me"
need to create a test group of "not me"











